if [ -d /usr/local/opt/go/libexec/bin ] ; then
    export PATH=$PATH:/usr/local/opt/go/libexec/bin
fi

mkdir -p "$HOME/go"
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

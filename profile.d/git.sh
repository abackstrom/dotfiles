if [ -f /usr/local/etc/bash_completion.d/git-completion.bash ] ; then
    . /usr/local/etc/bash_completion.d/git-completion.bash
fi

if [ -f /usr/local/etc/bash_completion.d/git-prompt.sh ] ; then
    . /usr/local/etc/bash_completion.d/git-prompt.sh
fi

export GIT_PAGER='less -R'
export GIT_PS1_SHOWUPSTREAM="verbose"
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWDIRTYSTATE=1

which brew &>/dev/null
if [ $? -eq 0 ]; then
    [[ -f `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh
fi

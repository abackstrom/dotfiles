# dotfiles

Install submodules (currently, Vim plugins):

    git submodule update --init --recursive

Setup YCM (Ubuntu):

    sudo apt-get install -y build-essential cmake python-dev
    bash .vim/bundle/YouCompleteMe/install.sh

Setup YCM (macOS + Homebrew):

    brew install cmake
    bash .vim/bundle/YouCompleteMe/install.sh

## Mac OS X Configuration

Disable [Character Accent Menu](http://osxdaily.com/2011/08/04/enable-key-repeat-mac-os-x-lion/):

    defaults write -g ApplePressAndHoldEnabled -bool false

## Mac Software

    brew install vim macvim reattach-to-user-namespace tmux the_silver_searcher
    brew linkapps

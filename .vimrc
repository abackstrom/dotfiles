" http://py.vaults.ca/~x/python_and_vim.html

set nocp
filetype off
filetype plugin indent on

let g:snips_author="Annika Backstrom"

set nocompatible
    \ backspace=2 softtabstop=0 tabstop=4 shiftwidth=4
    \ smarttab expandtab autoindent nocindent smartindent
    \ ruler nowrap hidden showmatch matchtime=3 t_Co=256
    \ wrapscan incsearch ignorecase hlsearch smartcase
    \ updatecount=50 autoread showcmd foldlevelstart=1
    \ modeline modelines=5 number spr scrolljump=5 scrolloff=3
    \ laststatus=2 nobackup nowritebackup

set nofoldenable
set foldmethod=indent
set foldnestmax=3

set rtp+=/usr/local/opt/fzf

let g:user_emmet_mode='a'

if executable('par')
    set formatprg=par
endif

set encoding=utf8
"set omnifunc=syntaxcomplete#Complete

" - Insert: F2 to toggle paste
" - Normal: F2 to switch to Insert (paste) mode
" - Leaving insert mode disables paste
set pastetoggle=<F2>
nnoremap <silent> <F2> :setlocal paste!<CR>i
autocmd InsertLeave <buffer> se nopaste

set guifont=Input:h13

" silver searcher
if executable('ag')
    set grepprg=ag\ --nogroup\ --nocolor
endif
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>:.cc<CR>
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
nnoremap \ :Ag<SPACE>

" disable ctrlp in quickfix
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>

" Show large "menu" with auto completion options
set wildmenu
set wildmode=list:longest
set wildignore+=tmp,.git,*.png,*.jpg,output,tmp-*

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.

map <F5> <Esc>:EnableFastPHPFolds<Cr>
map <F6> <Esc>:EnablePHPFolds<Cr>
map <F7> <Esc>:DisablePHPFolds<Cr>

set colorcolumn=120

" persistent undo
set undodir=~/.vim/undodir
set undofile

let mapleader=","
map <leader>x :XtermColorTable<CR>
map <leader>b :b#<CR>
map <leader>d :Bclose<CR>
map <leader>P P'[v']=
map <leader>p p'[v']=
map <leader>q gqap
map <leader>i :set list!<CR>
map <silent> <leader>S :%s/\s\+$//e<CR>:let @/=''<CR>
nnoremap <leader>m :silent make\|redraw!\|cc<CR>

map <silent> <leader>l :nohl<CR>
map <silent> <leader>L :se nu!<CR>

imap <C-y>. <Plug>snipMateNextOrTrigger
smap <C-y>. <Plug>snipMateNextOrTrigger

" fix j/k when wrapping is on
nmap j gj
nmap k gk

nnoremap <leader>[ :cp<CR>
nnoremap <leader>] :cn<CR>
nnoremap <leader>{ :cpf<CR>
nnoremap <leader>} :cnf<CR>

let g:ale_fixers = {
\   'go': ['gofmt'],
\}
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 1

set t_kD=[3~

" disable syntax highlighting in files > 100K
au BufReadPost * if getfsize(bufname("%")) > 100*1024 | set syntax= | endif

augroup wordpress
    autocmd!
    autocmd BufRead,BufNewFile */wp-content/*,*/wp-includes/* set noet nolist
augroup END

" autoindent after certain calls
autocmd BufRead,BufNewFile *.py set et
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
" remove whitespace at end of line
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``

au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead *.go set nolist noet

autocmd BufRead *.php,*.html set formatoptions=qrowcb
autocmd BufNewFile,BufRead *.tpl setfiletype html
autocmd BufReadPre php set foldlevel=1

autocmd FileType jinja,jinja.html set ts=2 sw=2

set suffixes=.bak,~,.o,.h,.info,.swp,.obj,.pyc

let g:solarized_termcolors=256
let g:solarized_termtrans=1
let g:solarized_bold=1
let g:solarized_underline=1
let g:solarized_italic=1

syntax enable
set background=dark
silent! colorscheme solarized
highlight clear SignColumn

let g:ycm_key_list_previous_completion = ['<Up>']
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_complete_in_comments = 1

source ~/.vim/rc/fzf

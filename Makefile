all:

TARGETS = $(shell cat manifest.txt)

.PHONY: submodules install all

all:
	@echo "You probably shouldn't run this file. If it fucks up"
	@echo "your day, don't complain to me."

submodules:
	git submodule update --init

install:
	for dotfile in $(TARGETS) ; do \
		ln -Trbs $$dotfile ../$$dotfile ; \
	done

vim: submodules
	vim +PluginInstall +qall
	sudo apt-get install -y build-essential cmake python-dev
	bash ./.vim/bundle/YouCompleteMe/install.sh

# fake modeline, so the vim directive doesn't get interpreted as a modeline:
# vim:

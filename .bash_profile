if [ -d "$HOME/bin" ] ; then         export PATH="$HOME/bin:$PATH" ; fi
if [ -d "$HOME/scripts/bin" ] ; then export PATH="$HOME/scripts/bin:$PATH" ; fi

export PATH=$PATH:/usr/local/opt/go/libexec/bin

export LESS=-SFX
export EDITOR=vim

export FZF_DEFAULT_COMMAND='fd --type f --hidden --exclude .git'

[ -f ~/.aws_keys ] && . ~/.aws_keys

#if hash keychain 2>/dev/null ; then
#    eval `keychain --eval --agents ssh id_rsa`
#fi

# history
export HISTCONTROL=erasedups:ignoreboth
export HISTIGNORE="pwsafe *:ls:*git push*"
export HISTSIZE=10000
shopt -s histappend cmdhist

hash brew &>/dev/null && if [ $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# Stolen from centos /etc/profile
for i in $HOME/dotfiles/profile.d/* ; do
    if [ -r "$i" ]; then
        if [ "${-#*i}" != "$-" ]; then
            . "$i"
        else
            . "$i" >/dev/null 2>&1
        fi
    fi
done

if [ ! -z "$PS1" ]; then
    _jobcount() { test $1 -gt 0 && echo [$1] ; }

    if [ "$(type -t __git_ps1 2>&1)" = "function" ]; then
        export PS1='\W$(__git_ps1 "(%s)")$(_jobcount \j)\$ '
    else
        echo "Could not find __git_ps1!"
        export PS1='\W$(_jobcount \j)\$ '
    fi
fi

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/annika/Downloads/google-cloud-sdk/path.bash.inc' ]; then source '/Users/annika/Downloads/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/annika/Downloads/google-cloud-sdk/completion.bash.inc' ]; then source '/Users/annika/Downloads/google-cloud-sdk/completion.bash.inc'; fi
